$(document).ready(function () {

	function subMenu() {
		let secondUl = $('.sidebar__header-menu-submenu');
		secondUl.parent().append('<span class="arrow"></span>');
		let liArrow = $('.menu .arrow');
		liArrow.on('click', function () {
			const $this = $(this);
			$this.parent().toggleClass('on');
		});
	}
	subMenu();

	function customSelect() {
		$('select').each(function () {
			var $this = $(this),
				numberOfOptions = $(this).children('option').length;

			$this.addClass('select-hidden');
			$this.wrap('<div class="select"></div>');
			$this.after('<div class="select-styled"></div>');

			var $styledSelect = $this.next('div.select-styled');
			$styledSelect.text($this.children('option:selected').text());

			var $list = $('<ul />', {
				'class': 'select-options'
			}).insertAfter($styledSelect);

			for (var i = 0; i < numberOfOptions; i++) {
				$('<li />', {
					text: $this.children('option').eq(i).text(),
					rel: $this.children('option').eq(i).val()
				}).appendTo($list);
			}

			var $listItems = $list.children('li');

			$styledSelect.click(function (e) {
				e.stopPropagation();
				$('div.select-styled.active').not(this).each(function () {
					$(this).removeClass('active').next('ul.select-options').hide();
				});
				$(this).toggleClass('active').next('ul.select-options').toggle();
			});

			$listItems.click(function (e) {
				e.stopPropagation();
				$styledSelect.text($(this).text()).removeClass('active');
				$this.val($(this).attr('rel'));
				$this.change();
				$list.hide();
			});

			$(document).click(function () {
				$styledSelect.removeClass('active');
				$list.hide();
			});

		});
	} customSelect();

	function mobileFilter() {
		$('.mobile-filter').on('click', function () {
			$('.transaction__content-pane-filter').toggleClass('on');
			$("body").addClass("hidden");
		});

		$('.transaction__content-pane-filter .close').on('click', function () {
			$('.transaction__content-pane-filter').removeClass('on');
			$("body").removeClass("hidden");
		});
	} mobileFilter();

	$(".burger-menu").on('click', function () {
		$(this).toggleClass("open");
		$("body").toggleClass("menu-on");
	});

	function download() {
		$('.mobile-btn').on('click', function () {
			$('.transaction__content-pane-files-block').toggleClass('on');
		});
		// close window
		$(document).click(function (event) {
			var target = $(event.target);

			if (!target.is('.mobile-btn') && !target.closest('.transaction__content-pane-files-block a').length) {
				$('.transaction__content-pane-files-block').removeClass('on');
			}
		});
	} download();

});
